<?php
   session_start();
   $username=$_SESSION['username'];
   $id=$_SESSION['id'];
   if(!$id) {
      header('Location:login.php');
   }
?>
<!doctype html>
<html>
<head>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<style media="screen">
		body {
			background-image: url(tree1.jpg);
			background-position: left;
			background-repeat: no-repeat;
			background-attachment: fixed;
		}
		a,
		a:hover,
		input {
		 color:black;
		 text-decoration:none;
		}
	</style>
</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- PROBLEME AVEC LE LOGO	-->
        <a class="navbar-brand" href="index.php"><span class="school">POP</span>School</a>
		   </div>
		   <div class="navbar-collapse" id="bs-example-navbar-collapse-1">
			   <ul class="nav navbar-nav">
           <li><a href="chat.php"><span class="menu">Chat</span></a></li>
          <li><a href="randomizer.php"><span class="menu">Random</span></a></li>
				   <li class="dropdown">
					   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">FILTRE<span class="caret"></span></a>
					   <ul class="dropdown-menu">
						   <li><a href="name.php">Popschoolers</a></li>
						   <li><a href="#">Date</a></li>
						   <li><a href="randomizer.php">Catégorie</a></li>
					   </ul>
				   </li>
			   </ul>
			   <ul class="nav navbar-nav navbar-right">
<?php
		include 'session.php';
		if ($logged==1) {
			echo "\t\t\t\t\t<li><a href='membre.php'>Espace membre</a></li>\n";
			echo "\t\t\t\t\t<li><a href='logout.php'>Déconnexion</a></li>\n";
		}
		elseif ($logged==2) {
			echo "\t\t\t\t\t<li><a href='login.php'>Connexion</a></li>\n";
			echo "\t\t\t\t\t<li><a href='register.php'>Inscription</a></li>\n";
		}
?>
			   </ul>
		   </div>
	   </div>
   </nav>

   <div class="container">
      <div class="row">
<?php

   if(isset($_GET['id'])) {
      if($_GET['id']==$id) {
         if(isset($_POST['submit'])) {
            $handle=mysqli_connect("localhost","root","1234","veilleApp") or die('Erreur de connexion a la base de données');
            $query="DELETE FROM users WHERE id='$id'";
            $sql="DELETE FROM veille WHERE id_user='$id'";
            $sql1="DELETE FROM chat WHERE username='$username'";
            $sql2="DELETE FROM comment WHERE id_user='$id'";
            $sql3="DELETE FROM popularity WHERE id_user='$id'";

            $result=mysqli_query($handle,$query);
            $req=mysqli_query($handle,$sql);
            $req1=mysqli_query($handle,$sql1);
            $req2=mysqli_query($handle,$sql2);
            $req3=mysqli_query($handle,$sql3);
            header('Location:logout.php');
         } else {
            echo "<form action='delete_profil.php?id=".$_GET['id']."' method=post>";
            echo "<p class='col-xs-6 col-xs-offset-2'>Etes vous sur de vouloir supprimer votre compte ainsi que tout ce qu'il contient ?</p>";
            echo "<input class='col-xs-1' name='submit' type=submit value='confirmer'>";
            echo "</form>";
            echo "<a href='membre.php' class='col-xs-1'><input type=submit value='Not today'></a>";
         }
     } else {
       header('Location:index.php');
     }
  } else {
    header('Location:index.php');
  }

?>
      </div>
   </div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="bootstrap/js/bootstrap.js"></script>

</body>
</html>
