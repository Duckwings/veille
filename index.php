<!doctype html>
<html>
<head>
   <title>Page d'accueil</title>
   <meta charset>
   <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="style.css">
   <style media="screen">
	   body {
		   background-image: url(tree1.jpg);
		   background-position: left;
		   background-repeat: no-repeat;
		   background-attachment: fixed;
	   }
	   img {
		   max-width:60px;
		   height: 60px;
		   border-radius: 2px;
		   margin-bottom: 4px;
		   float: left;
		   margin-right: 15px;
		   background-color: #F0F6C6;
		   padding: 3px;
		   border-radius: 2px;
		   border: 1px #85BEB8 solid;
	   }
   </style>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- PROBLEME AVEC LE LOGO	-->
            <a class="navbar-brand" href="index.php"><span class="school">POP</span>School</a>
		   </div>
		   <div class="navbar-collapse" id="bs-example-navbar-collapse-1">
			   <ul class="nav navbar-nav navbar-left">
					 <li><a href="chat.php"><span class="menu">Chat</span></a></li>
					 <li><a href="randomizer.php"><span class="menu">Random</span></a></li>
				    <li class="dropdown">
					   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">FILTRE<span class="caret"></span></a>
					   <ul class="dropdown-menu">
						   <li><a href="name.php">Popschoolers</a></li>
						   <li><a href="#">Date</a></li>
						   <li><a href="key.php">Catégorie</a></li>
					   </ul>
				   </li>
			   </ul>
			   <ul class="nav navbar-nav navbar-right">
<?php
		include 'session.php';
		if ($logged==1) {
			echo "\t\t\t\t\t<li><a href='membre.php'>Espace membre</a></li>\n";
			echo "\t\t\t\t\t<li><a href='logout.php'>Déconnexion</a></li>\n";
		}
		elseif ($logged==2) {
			echo "\t\t\t\t\t<li><a href='login.php'>Connexion</a></li>\n";
			echo "\t\t\t\t\t<li><a href='register.php'>Inscription</a></li>\n";
		}
?>
			   </ul>
		   </div>
	   </div>
   </nav>
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="bonjour">
<?php
                  session_start();
                  echo "\t\t\t\t<h3>BONJOUR <span class='pseudo'>".$username."</span></h3>\n";
                  include 'date.php';
                  echo "\t\t\t\tLES VEILLE DU JOUR : <span class='dark'>".$dday." ".$jour." ".$month." ".$annee."</span><br>\n";
?>
            </div>
         </div>
      </div>
      <div class="row">
<?php
		$handle=mysqli_connect("localhost","root","1234","veilleApp") or die('Error');
		$query="SELECT * FROM ( SELECT * FROM veille ORDER BY date desc ) v GROUP BY v.id_user";
		$result=mysqli_query($handle,$query);

		while($line=mysqli_fetch_array($result)) {
      $id_user=$line['id_user'];
      $id_veille=$line['id'];

      echo "\t\t<div class='col-md-3'>\n";
		echo "\t\t\t<a href='veille.php?id=".$line['id']."'>\n";
      echo "\t\t\t<div id='veille'>\n";

      $query="SELECT * FROM users WHERE id='$id_user'";
      $user_r=mysqli_query($handle,$query);
      $line_user=mysqli_fetch_array($user_r);

      $query="SELECT *, DATE_FORMAT(veille.date, '%d/%m à %H:%i') as heure FROM veille WHERE id='$id_veille'";
      $veille_r=mysqli_query($handle,$query);
      $line_veille=mysqli_fetch_array($veille_r);
			echo "\t\t\t\t<div class='id'>\n";
			echo "\t\t\t\t\t<div class='row'>\n";
			echo "\t\t\t\t\t\t<div class='col-md-4'>\n";
			echo "\t\t\t\t\t\t\t<img  src='../uploads/".$line_user["img"]."'>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t\t<div class='col-md-8'>\n";
			echo "\t\t\t\t\t\t\t<h4 class='nom'>".$line_user['firstname']." ".$line_user['name']."</h4>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t</div>\n";
			echo "\t\t\t\t</div>\n";
			echo "\t\t\t\t<div class='subject'>\n";
			echo "\t\t\t\t\t<div class='row'>\n";
			echo "\t\t\t\t\t\t<div class='col-md-3'>\n";
			echo "\t\t\t\t\t\t\t<p class='sujet'><span class='titre'>SUJET  </span></p>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t\t<div class='col-md-9'>\n";

			$title = $line['title'];
			if(strlen($title) > 60){
				$title = substr($title, 0, 60) . "...";
			}
			echo "\t\t\t\t\t\t\t".$title."\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t</div>\n";
			echo "\t\t\t\t</div>\n";
			echo "\t\t\t\t<div class='row'>\n";
			echo "\t\t\t\t\t<div class='col-md-12'>\n";
			echo "\t\t\t\t\t\t<div class='heure'>\n";
			//$date = date('H:i:s', strtotime($line_veille['date']));
			echo "\t\t\t\t\t\t\t<p class='dark'>Postée le ". $line_veille['heure'] ."</p>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t</div>\n";
			echo "\t\t\t\t</div>\n";
			echo "\t\t\t</div>\n";
			echo "\t\t\t</a>\n";
			echo "\t\t</div>\n";

	}
?>
      </div>
   </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="bootstrap/js/bootstrap.js"></script>
</body>
</html>
