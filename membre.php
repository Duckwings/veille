<?php
session_start();

$username=$_SESSION['username'];
$id=$_SESSION['id'];
if ($id) {
  if (isset($_GET["id"]) && $_GET["id"] != $id) {
    $permitted=0;
    $id=$_GET["id"];
  }
  elseif (isset($_GET["id"]) && $_GET["id"]==$id) {
    $permitted=1;
  }
  else {
    $permitted=1;
  }
} else {
  if (isset($_GET['id'])) {
    $permitted=0;
    $id=$_GET["id"];
  } else {
    header('Location:login.php');
  }
}
?>
<!doctype html>
<html>
<head>
  <title>Page membre</title>
  <meta charset>
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="style.css">
   <style>
    form {
      padding:20px 0 0 0;
      margin:0;
    }
    .cancel {
padding:0;
    }
   </style>

</head>
<body>
<?php include('menu.php'); ?>
<div class="container">
  <div id="bonjour">
    <!--<?php

    if ($permitted==1) {
    echo "<h4>Bonjour ".$_SESSION['username']."</h4>";
  }

  ?>-->
</div>
<div id="espace">
  <div class="modifier">
    <div class="row">
      <div class="col-xs-8 col-xs-offset-2 ">
<?php

     $handle=mysqli_connect("localhost","root","1234","veilleApp") or die('Error');
     $query = "SELECT * FROM users WHERE id='$id'";
     $result = mysqli_query($handle,$query);

     if($handle->affected_rows > 0) {
       $line=mysqli_fetch_array($result);
       $username=$line["username"];
       $name=$line["name"];
       $f_name=$line["firstname"];
       $promo=$line["promo"];
       $id=$line["id"];
       $pseudo=$line['username'];
       //if(!empty($_GET['tri']) && $_GET['tri'])
       $query = "SELECT * FROM veille WHERE id_user='$id'";
       $result = mysqli_query($handle,$query);
       $nbv=$result->num_rows;
       if ($permitted==1) {
?>
         <div class="row">
            <div class="col-xs-8">
               <h3> Mon profil </h3>
            </div>
            <div class="col-xs-4">
               <h2><span class='light'><a href='profil.php'>Modifier</a> - <a href='delete_profil.php?id=<?php echo $id?>'>Supprimer</a></span></h2>
            </div>
         </div>
<?php
          }
          if ($permitted==0) {
            echo "<h3>".$username."</h3>";

          }
?>
         </div>
      </div>
    </div>
    <div id="profil">
      <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
          <div class="row">
            <div class="col-xs-3">
              <?php
              echo " <img class='thumbnail' src='../uploads/".$line["img"]."'>";
              echo"</div>";
              echo"<div class='col-xs-5'>\n";

              echo "\t\t\t\t<ul>\n";
              echo "\t\t\t\t\t<li class='fat'>".$f_name." ".$name."</li>\n";
              echo "\t\t\t\t\t<li><span class='light'>Promo :</span> ".$promo."</li>\n";
              echo "\t\t\t\t\t<li><span class='light'>Pseudo : </span>".$pseudo."</li>\n";
              echo "\t\t\t\t\t<li><span class='light'>Veilles postées : </span>".$nbv."</li>\n";
              echo "\t\t\t\t\t<li><span class='light'>Veille favorite :</span> </li>\n";
              echo "\t\t\t\t</ul>\n";
              echo "\t\t\t</div>\n";
              echo "\t\t<div class='col-xs-4'>\n";
            }

            if ($permitted==1) {
              echo "\t\t\t<a href='day.php'><button type='button' class='btn btn-primary'>Ajouter une veille</button></a>\n";
            }
            ?>
          </div>
        </div>
      </div>

    </div>
  </div>
  <div id="content">
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
         <div class="row">
            <div class="col-xs-8">
               <h3> Toutes les veilles </h3>
            </div>
            <div class="col-xs-3">
<?php
      $query="SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee  FROM ( SELECT * FROM veille ORDER BY date desc ) v WHERE v.id_user='$id'";
      $result=mysqli_query($handle,$query);
      if(isset($_GET["search"])){
         $s=$_GET['search'];
         $search=explode(" ",$s);

         $query="SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee  FROM (SELECT * FROM veille";
         $i=0;
         foreach($search as $mot) {
            if($i==0) {
               $query.="  WHERE title LIKE '%$mot%' OR subject LIKE '%$mot%' OR keyword LIKE '%$mot%' OR date LIKE '%$mot%' ORDER BY date desc";
            }
            else {
               $query.="  OR title LIKE '%$mot%' OR subject LIKE '%$mot%' OR keyword LIKE '%$mot%' OR date LIKE '%$mot%' ORDER BY date desc";
            }
            $i++;
            $query.=") v WHERE v.id_user='$id'";
         }
         //echo $sql;
         $result = mysqli_query($handle,$query);

      }

      echo "\t\t\t\t<form action='membre.php' method='get'>\n";
      if(isset($_GET['id'])) {
         echo "\t\t\t\t\t<input type='hidden' name='id' value='".$id."'>\n";
      }
      echo "\t\t\t\t\t<input autocomplete='off' class='form-control' tabindex='1' name='search' placeholder='Rechercher' title='Rechercher dans Veille' value='".$s."'>";

      echo "\t\t\t\t</form>\n";

      echo "\t\t\t</div>\n";
      echo "\t\t\t<div class='col-xs-1 cancel'>\n";
      if(isset($_GET['search'])) {
         if(isset($_GET['id'])) {
            echo "<a class='cancel' href='membre.php?id=".$id."'>X</a>\n";
         } else {
            echo "<a class='cancel' href='membre.php'>X</a>\n";
         }
      }
      echo "\t\t\t</div>\n";
      echo "\t\t</div>\n";


      $i=$nbv+1;
      if($handle->affected_rows > 0) {
         while($line=mysqli_fetch_array($result)) {
            $i--;

            echo "\t\t<div id='veille_membre'>\n";
            echo "\t\t\t<div class='row'>\n";
            echo "\t\t\t\t<div class='col-xs-2'>\n";
            echo "\t\t\t\t\t<p class='dark'>" .$line['date_formatee']."</p>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t\t<div class='col-xs-7'>\n";
            echo "\t\t\t\t\t<div class='subject'>\n";

            $title = $line['subject'];
            if(strlen($title) > 60){
               $title = substr($title, 0, 60) ."...";
            }
            echo "\t\t\t\t\t\t<p class='titre'> <a href='veille.php?id=".$line['id']."'><img class='sujet_img' src='sujet.png'>   " . $title." </a></p>\n";
            echo "\t\t\t\t\t\t<p> <img class='key_img' src='key.png'> ".$line['keyword']."</p>\n";
            echo "\t\t\t\t\t</div>\n";
            echo "\t\t\t\t</div>\n";

            if ($permitted==1){
               echo "\t\t\t\t<div class='col-xs-1'>\n";
               echo "\t\t\t\t\t<a href='veille.php?id=".$line['id']."'><img class='go' src='go.png'></a>\n";
               echo "\t\t\t\t</div>\n";
               echo "\t\t\t\t<div class='col-xs-1'>\n";
               echo "\t\t\t\t\t<a href='delete_veille.php?id=".$line['id']."'><img class='delete' src='delete.png'></a>\n";
               echo "\t\t\t\t</div>\n";
               echo "\t\t\t\t<div class='col-xs-1'>\n";
               echo "\t\t\t\t\t<a href='update_veille.php?id=".$line['id']."'><img class='update' src='update.png'></a>\n";
               echo "\t\t\t\t</div>\n";
            } elseif ($permitted==0) {
               echo "\t\t\t\t<div class='col-xs-2 col-xs-offset-1'>\n";
               echo "\t\t\t\t\t<a href='veille.php?id=".$line['id']."'><img class='go' src='go.png'></a>\n";
               echo "\t\t\t\t</div>\n";
            }
            echo "\t\t\t</div>\n";
            echo "\t\t</div>\n";
         }
      } else {
         echo "\t\t\t<p>Aucune veille n'a été postée pour le moment...</p>\n";
      }

      ?>
       </div>
     </div>
   </div>
  </div>
 </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.js"></script>

</body>
</html>
