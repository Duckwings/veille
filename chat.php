<?php
// On démarre la session AVANT d'écrire du code HTML
session_start();
$handle=mysqli_connect("localhost","root","1234","veilleApp") or die('Erreur de connexion a la base de données');
$username=$_SESSION['username'];
$query="SELECT * FROM chat";
$result=mysqli_query($handle,$query);
$line=mysqli_fetch_array($result);

?>
<!DOCTYPE html>
<html>
<head>
<style media="screen">
  ul li{
    margin:0;
  }
</style>
  <meta charset="utf-8" />
  <title>Mini-chat</title>
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<?php include('menu.php'); ?>
   <div class="container">
      <div class="chat">
         <div class="chat_title">
            <h3>Espace Chat</h3>
         </div>
<?php
// Connexion à la base de données
if (isset($_POST['submit'])) {
  # code...
         $handle=mysqli_connect("localhost","root","1234","veilleApp") or die('Error');
         $query="INSERT INTO chat (username,message) VALUES ('$username',\"" . $_POST["message"] . "\")";
         $result=mysqli_query($handle,$query);

         // Redirection du visiteur vers la page du minichat
         header('Location: chat.php');

       }

// Récupération des 10 derniers messages
$query="SELECT * FROM chat ORDER BY id DESC LIMIT 0, 100";
// Affichage de chaque message (toutes les données sont protégées par htmlspecialchars)
$result=mysqli_query($handle,$query);

?>
         <div class="zone">
<?php
      echo "\t\t\t<ul>\n";
      while($line=mysqli_fetch_array($result)) {
         echo "\t\t\t\t<li>\n";
         echo "\t\t\t\t\t<strong>".$line["username"]." : ". "</strong>\n";
         echo "\t\t\t\t\t".$line["message"]."\n";
         echo "\t\t\t\t</li>\n";
      }
      echo "\t\t\t</ul>\n";
?>
         </div>
      </div>
      <div class="message">
         <form action="chat.php" method="post">
            <label for="message">Message</label> :  <input type="text" name="message" id="message" />
            <input type="submit" name="submit" value="ENVOYER" />
         </form>
      </div>
   </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="bootstrap/js/bootstrap.js"></script>
</body>
</html>
