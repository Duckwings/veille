<!doctype html>
<html>
<head>
	<title>Nouvelle veille</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>

<?php include('menu.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="bienvenue">

<?php
  session_start();
  include 'date_insert.php';
  $handle=mysqli_connect("localhost","root","1234","veilleApp") or die('Erreur de connexion a la base de données');
  $username=$_SESSION['username'];
  $id=$_SESSION['id'];


   //Declare les variable a base des POST
   $subject=strip_tags($_POST["subject"]);
   $title=strip_tags($_POST["title"]);
   $keyword=strip_tags($_POST["keyword"]);
   $date=strip_tags($_POST["date"]);
   $id_user=strip_tags($_POST["id_user"]);


   //Le code ne s'applique que si le boutton Ajouter est appuyé
   if(isset($_POST["submit"])){
      if($subject&&$title&&$keyword&&$date&&$id_user) {
         if (isset($_FILES['content']) && $_FILES['content']['error'] == 0) {
            if ($_FILES['content']['size'] <= 10000000) {
               $info= pathinfo($_FILES['content']['name']);
               $extension_info=$info['extension'];
               $extensions=array('jpg', 'jpeg', 'png', 'odt', 'txt', 'md', 'pdf', 'html', 'mp4');
               if (in_array($extension_info, $extensions)) {
                  $content=basename($_FILES['content']['tmp_name']).".".$extension_info;
                  move_uploaded_file($_FILES['content']['tmp_name'], '../uploads/'.$content);
                  $query="INSERT INTO `veille` (id_user,date,subject,keyword,title) VALUES ('$id_user','$date',\"$subject\",'$keyword',\"$title\")";
                  $result = mysqli_query($handle,$query);
                  $sql="SELECT * FROM veille WHERE id_user='$id_user' AND date='$date' AND subject=\"$subject\" AND keyword='$keyword' AND title=\"$title\"";
                  $id_req=mysqli_fetch_array(mysqli_query($handle,$sql));
                  $id_veille=$id_req['id'];
                  $sql2="INSERT INTO contents (id_veille,nom,type) VALUES ('$id_veille','$content','$extension_info')";
                  $req=mysqli_query($handle,$sql2);

                  if($handle->affected_rows > 0) {
                     header('Location:membre.php');
                  } else {
                     echo "<p class='error'>* Une erreur est survenue, veuillez recommencer...</p>";
                     }
               } else {
                  echo "<p class='error'>* Format de fichier non pris en charge</p>";
                  }
            } else {
               echo "<p class='error'>* La taille du fichier est trop importante</p>";
               }
         } else {
            $query="INSERT INTO `veille` (id_user,date,subject,keyword,title) VALUES ('$id_user','$date',\"$subject\",'$keyword',\"$title\")";
            $result = mysqli_query($handle,$query);
            if($handle->affected_rows > 0) {
               header('Location:membre.php');
            } else {
               echo "<p class='error'>* Une erreur est survenue, veuillez recommencer...</p>";
               }
            }

      } else {
         echo "<p class='error'>* Veuillez renseigner tout les champs </p>";
       }
   }


?>

      <h3>Ajouter ma veille</h3>
      <form action="day.php" method="POST" enctype="multipart/form-data">
         <div class="form-group">
            <label for="subject">Sujet</label>
            <input class="champ form-control" tabindex="1" type="text" name="subject" placeholder="Subject..."><br>
         </div>
   		<div class="form-group">
            <label for="title">Titre</label>
            <input class="champ form-control" tabindex="2" type="text" name="title" placeholder="Title..."><br>
         </div>
   		<div class="form-group">
		<label>Catégories</label>
		<select name="keyword" tabindex="3" class="form-control">
			<option value="Web">Web</option>
			<option value="Objets connecté">Objets connecté</option>
			<option value="Logiciel">Logiciel</option>
			<option value="Hacks">Hacks</option>
			<option value="Robotique">Robotique</option>
			<option value="Autres">Autres</option>
		</select>
         </div>
            <input  type="hidden" name="date" value="<?php echo $year."/".$mois."/".$jour."/".$heure."/".$minute; ?>">
   		<div class="form-group">
            <label for="contenu">Contenu</label>

            <input class="champ" tabindex="4" type="file" name="content"><br>
         </div>
            <input type="hidden" name="id_user" value="<?php echo $id; ?>">
            <input type="submit" tabindex="5" name="submit" class="btn btn-info" value="Ajouter">
      </form>
   </div>
   </body>
</html>
