<!doctype html>
<html>
<head>
  <title>Filtre catégories</title>
  <meta charset>
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="style.css">
  <style>
   .main_row {
      margin-top:50px;
   }
  </style>
</head>
<body>
<?php include('menu.php'); ?>
   <div class="container">
      <div class="row">
         <div class="col-xs-2">
            <h3>Catégories : </h3>
		   </div>
         <form action="key.php" method="post">
            <div id="form" class="col-xs-3 col-xs-offset-5">
            	<select name="keyword" tabindex="1" class="form-control">
			         <option value="Web">Web</option>
			         <option value="Objets connecté">Objets connecté</option>
			         <option value="Logiciel">Logiciel</option>
			         <option value="Hacks">Hacks</option>
			         <option value="Robotique">Robotique</option>
			         <option value="Autres">Autres</option>
		      </select>
		      </div>
		      <div class="col-xs-2">
               <input type="submit" tabindex="2" name="submit" class="btn btn-info" value="Sélectionner">
		      </div>
		   <!--   <div class="col-xs-1" id="cancel">
               <input type="submit" tabindex="3" name="cancel" class="btn btn-info" value="Cancel">
		      </div>   -->
         </form>
      </div>
      <div class="row main_row">
         <div class="col-xs-12 col-md-8 col-md-offset-2">
<?php
   $handle=mysqli_connect("localhost","root","1234","veilleApp") or die('Erreur de connexion a la base de données');
   if(isset($_POST['submit'])) {
        $keyword=$_POST['keyword'];
        $query = "SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee FROM veille WHERE keyword='$keyword'";
        $result = mysqli_query($handle,$query);
   } elseif(isset($_POST['cancel'])) {
      header('Location:key.php');       
   } else {
        $query = "SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee FROM veille";
        $result = mysqli_query($handle,$query);
   }
   if($handle->affected_rows > 0) {
      while($line=mysqli_fetch_array($result)) {
         $i--;
         
         echo "\t\t<div id='veille_membre'>\n";
         echo "\t\t\t<div class='row'>\n";
         echo "\t\t\t\t<div class='col-xs-2'>\n";
         echo "\t\t\t\t\t<p class='dark'>" .$line['date_formatee']."</p>\n";
         echo "\t\t\t\t</div>\n";
         echo "\t\t\t\t<div class='col-xs-7'>\n";
         echo "\t\t\t\t\t<div class='subject'>\n";
         $title = $line['subject'];
         if(strlen($title) > 60){
            $title = substr($title, 0, 60) ."...";
         }
         echo "\t\t\t\t\t\t<p class='titre'> <a href='veille.php?id=".$line['id']."'><img class='sujet_img' src='sujet.png'>   " . $title." </a></p>\n";
         echo "\t\t\t\t\t\t<p> <img class='key_img' src='key.png'> ".$line['keyword']."</p>\n";
         echo "\t\t\t\t\t</div>\n";
         echo "\t\t\t\t</div>\n";
         echo "\t\t\t\t<div class='col-xs-2 col-xs-offset-1'>\n";
         echo "\t\t\t\t\t<a href='veille.php?id=".$line['id']."'><img class='go' src='go.png'></a>\n";
         echo "\t\t\t\t</div>\n";
         echo "\t\t\t</div>\n";
         echo "\t\t</div>\n";
      }
   } else {
      echo "\t\t\t<p>Aucune veille n'a été postée pour le moment...</p>\n";
   }
?>
       </div>
     </div>
   </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="bootstrap/js/bootstrap.js"></script>
</body>
</html>
